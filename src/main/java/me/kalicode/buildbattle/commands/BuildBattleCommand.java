package me.kalicode.buildbattle.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.kalicode.buildbattle.WaitingRoom;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;

public class BuildBattleCommand implements CommandExecutor {
    private WaitingRoom waitingRoom;

    public BuildBattleCommand(JavaPlugin plugin) {
        waitingRoom = new WaitingRoom(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sendInfo(sender, command, label, args);

        if (command.getName().equalsIgnoreCase("buildbattle")) {
            return commandIsBuildBattle(sender, args);
        }

        return false;

        /*if (!command.getName().equalsIgnoreCase("buildbattle")) {
            return false;
        }

        if (args.length != 1) {
            sender.sendMessage(Component.text("Usage: /buildbattle join|leave|end",
                NamedTextColor.DARK_AQUA));
            return false;
        }

        BuildBattleRunnable buildBattle = waitingRoom.getBuildBattle();

        if (args[0].equalsIgnoreCase("end") && buildBattle != null) {
            buildBattle.end();
            return true;
        }

        if (!(sender instanceof Player)) {
            return false;
        }

        Player player = (Player) sender;

        if (args[0].equalsIgnoreCase("join")) {
            waitingRoom.offerJoin(player);
            return true;
        }

        if (args[0].equalsIgnoreCase("leave")) {
            waitingRoom.offerLeave(player);
        }

        return false;*/
    }

    private void sendInfo(CommandSender sender, Command command, String label, String[] args) {
        StringBuilder sb = new StringBuilder("Command Info:\n");
        sb.append("Sender Name: ");
        sb.append(sender.getName());
        sb.append("\nCommand Name: ");
        sb.append(command.getName());
        sb.append("\nLabel: ");
        sb.append(label);
        sb.append("\nArgs:");
        for (String arg : args) {
            sb.append(" ");
            sb.append(arg);
        }

        sender.sendMessage(Component.text(sb.toString()));
    }

    private boolean commandIsBuildBattle(CommandSender sender, String[] args) {
        if (args.length == 1) {
            return senderUsedAnArgument(sender, args[0]);
        } else {
            sender.sendMessage(Component.text("Usage: /buildbattle join|leave|end",
                NamedTextColor.DARK_AQUA));
            return false;
        }
    }

    private boolean senderUsedAnArgument(CommandSender sender, String arg) {
        if (arg.equalsIgnoreCase("end")) {
            return argumentIsEnd(sender);
        } else if (sender instanceof Player) {
            return senderIsPlayer((Player) sender, arg);
        }

        return false;
    }

    private boolean argumentIsEnd(CommandSender sender) {
        if (!waitingRoom.isBuildBattleEmpty()) {
            waitingRoom.getBuildBattle().end();
            return true;
        }

        return false;
    }

    private boolean senderIsPlayer(Player player, String arg) {
        if (arg.equalsIgnoreCase("join")) {
            return waitingRoom.offerJoin(player);       
        } else if (arg.equalsIgnoreCase("leave")) {
            return waitingRoom.offerLeave(player);
        }

        return false;
    }
}