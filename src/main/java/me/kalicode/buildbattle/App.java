package me.kalicode.buildbattle;

import org.bukkit.plugin.java.JavaPlugin;
import me.kalicode.buildbattle.commands.BuildBattleCommand;

public final class App extends JavaPlugin {
    @Override
    public void onEnable() {
        System.out.println("Build Battle has started");

        super.saveDefaultConfig();
        super.getCommand("buildbattle").setExecutor(new BuildBattleCommand(this));
    }

    @Override
    public void onDisable() {
        System.out.println("Build Battle has stopped");
    }
}
