package me.kalicode.buildbattle.runnables;

import java.time.Duration;
import java.util.LinkedList;
import java.util.Queue;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import me.kalicode.buildbattle.WaitingRoom;
import net.kyori.adventure.bossbar.BossBar;
import net.kyori.adventure.bossbar.BossBar.Color;
import net.kyori.adventure.bossbar.BossBar.Overlay;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.title.Title;
import net.kyori.adventure.title.Title.Times;

public class BuildBattleRunnable extends BukkitRunnable {
    private Queue<Player> players = new LinkedList<>();
    private String theme;
    private BossBar bossBar;
    private int secondsRunning;

    public BuildBattleRunnable(WaitingRoom waitingRoom, Location location, String theme) {
        this.theme = theme;
        this.bossBar = BossBar.bossBar(Component.empty(), 1.0f, Color.GREEN, Overlay.PROGRESS);

        for (int i = 0; i < 4 && i < waitingRoom.size(); i++) {
            Player player = waitingRoom.remove();
            player.sendMessage(Component.text("Joining the build battle", NamedTextColor.GOLD));
            player.teleport(location);
            player.setGameMode(GameMode.CREATIVE);
            player.showTitle(Title.title(
                Component.text(theme, NamedTextColor.GOLD),
                Component.text("Theme", NamedTextColor.GOLD),
                Times.of(Duration.ofSeconds(2), Duration.ofSeconds(2), Duration.ofSeconds(2))));
            player.showBossBar(bossBar);
            
            players.offer(player);
        }
    }

    public boolean offerLeave(Player player) {
        if (players.remove(player)) {
            cancelIfEmpty();
            leave(player);
            return true;
        }

        return false;
    }

    @Override
    public void run() {
        if (secondsRunning < 30) {
            bossBar.progress(1.0f - secondsRunning / 30.0f);
            bossBar.name(Component.text(theme, NamedTextColor.GOLD)
                .append(Component.text(" starts in ", NamedTextColor.GOLD))
                .append(Component.text(30 - secondsRunning, NamedTextColor.GOLD))
                .append(Component.text(" seconds")));
        } else if (secondsRunning < 90) {
            int secondsRunningAfterStart = secondsRunning - 30;
            bossBar.progress(1.0f - secondsRunningAfterStart / 60.0f);
            bossBar.name(Component.text(theme, NamedTextColor.GOLD)
                .append(Component.text(" ends in ", NamedTextColor.GOLD))
                .append(Component.text(1 - secondsRunningAfterStart / 60, NamedTextColor.GOLD))
                .append(Component.text(" minutes")));
        } else {
            end();
        }

        ++secondsRunning;
        /*BossBar bossBar = buildBattle.getBossBar();

        if (secondsRunning < 30) {
            bossBar.progress(1.0f - secondsRunning / 30.0f);
            bossBar.name(Component.text("Starting in ")
                .append(Component.text(30 - secondsRunning))
                .append(Component.text(" seconds")));
        } else if (secondsRunning < 90) {
            int seconds = secondsRunning - 30;
            int minutes = seconds / 60;
            
            bossBar.progress(1.0f - seconds / 60.0f);
            bossBar.name(Component.text("Modern: Ends in ")
                .append(Component.text(1 - minutes))
                .append(Component.text(" minutes")));
        } else {
            buildBattle.end();
            
            this.cancel();
        }

        ++secondsRunning;*/
    }

    public void end() {
        super.cancel();

        for (int i = 0; i < players.size(); i++) {
            leave(players.remove());
        }
    }

    private void leave(Player player) {
        player.sendMessage(Component.text("Leaving the build battle", NamedTextColor.GOLD));
        player.hideBossBar(bossBar);
    }

    private void cancelIfEmpty() {
        if (players.size() < 1) {
            super.cancel();
        }
    }
}