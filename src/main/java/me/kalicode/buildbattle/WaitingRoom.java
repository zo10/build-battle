package me.kalicode.buildbattle;

import java.util.LinkedList;
import java.util.Queue;

import javax.annotation.Nullable;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.kalicode.buildbattle.runnables.BuildBattleRunnable;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;

public class WaitingRoom {
    private Queue<Player> players = new LinkedList<>();
    private JavaPlugin plugin;
    private Location location;
    private BuildBattleRunnable buildBattleRunnable;

    public WaitingRoom(JavaPlugin plugin) {
        this.plugin = plugin;
        this.location = plugin.getConfig().getLocation("waiting-room-location");
    }

    public boolean offerJoin(Player player) {
        if (players.contains(player)) {
            player.sendMessage(Component.text("You are already in the waiting room",
                NamedTextColor.DARK_AQUA));
            return false;
        } else {
            join(player);
            return true;
        }
    }

    public void join(Player player) {
        players.offer(player);
        
        player.sendMessage(Component.text("Joining the waiting room",
            NamedTextColor.GOLD));
        player.teleport(location);
        player.setGameMode(GameMode.SPECTATOR);

        proposeStartBuildBattle();
    }

    public boolean offerLeave(Player player) {
        if (players.remove(player)) {
            player.sendMessage(Component.text("Leaving the waiting room", NamedTextColor.GOLD));
            player.setGameMode(GameMode.CREATIVE);
            return true;
        } else if (buildBattleRunnable != null && buildBattleRunnable.offerLeave(player)) {
            return true;
        } else {
            player.sendMessage(Component.text("You are not in the waiting room or the build battle",
                NamedTextColor.DARK_AQUA));
            return false;
        }
    }

    public int size() {
        return players.size();
    }

    public Player remove() {
        return players.remove();
    }

    @Nullable
    public BuildBattleRunnable getBuildBattle() {
        return buildBattleRunnable;
    }

    public boolean isBuildBattleEmpty() {
        return buildBattleRunnable == null || buildBattleRunnable.isCancelled();
    }

    private void proposeStartBuildBattle() {
        if (buildBattleRunnable == null || buildBattleRunnable.isCancelled()) {
            buildBattleRunnable = new BuildBattleRunnable(this,
                plugin.getConfig().getLocation("build-battle-location"),
                plugin.getConfig().getStringList("themes").get(0));
            buildBattleRunnable.runTaskTimer(plugin, 0, 20);
        }
    }
}